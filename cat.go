package main

import (
	"fmt"
	"io/ioutil"
)

func catBuiltin(list []string) string {
	var s string
	if len(list) == 1 {
		s = fmt.Sprintln("cat needs an argument")
		return s
	}
	for i := 1; i < len(list); i++ {
		dat, err := ioutil.ReadFile(list[i])
		if err != nil {
			s = fmt.Sprintf("Couldn't open file %s", list[i])
		}
		s = fmt.Sprint(string(dat))

	}
	return s
}
