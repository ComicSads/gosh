package main

import "gitlab.com/ComicSads/neko/neko"

func nekoBuiltin(list []string) string {
	var trueOutput, newline bool
	newline = true
	var printable []string

	var argsDone bool
	for i := 1; i < len(list); i++ {
		switch list[i] {
		case "-t":
			if argsDone {
				printable = append(printable, list[i])
				continue
			}
			trueOutput = true
		case "-n":
			if argsDone {
				printable = append(printable, list[i])
				continue
			}
			newline = false
		case "--":
			argsDone = true
		default:
			printable = append(printable, list[i])
		}
	}
	neko.Setup(trueOutput, newline)
	return neko.Sprint(printable...)
}
