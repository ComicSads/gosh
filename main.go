package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"strings"
)

type command struct {
	help string
	name string
}

var builtins []command
var commands []string

func startup() {
	builtins = append(builtins, command{name: "neko", help: "echo replacement"})
	builtins = append(builtins, command{name: "exit", help: "exit shell"})
	builtins = append(builtins, command{name: "ls", help: "lists files in directory"})
	builtins = append(builtins, command{name: "cd", help: "changes directory"})
	builtins = append(builtins, command{name: "pwd", help: "prints path"})
	builtins = append(builtins, command{name: "type", help: "prints where command is run"})
	builtins = append(builtins, command{name: "cat", help: "reads a files contents"})
	builtins = append(builtins, command{name: "alias", help: "allows you to create aliases"})
	builtins = append(builtins, command{name: "help", help: "provides help on what a command does"})
	for _, v := range builtins {
		commands = append(commands, v.name)
	}

	config()
}

func main() {
	startup()
	_, err := fmt.Print()
	workingDir, err = os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	r := bufio.NewReader(os.Stdin)
	fmt.Println("Gosh shell v0.0.1")
	fmt.Println("------------")

	for {
		prompt(1)
		text, _ := r.ReadString('\n')
		text = strings.Replace(text, "\r\n", "\n", -1)
		text = strings.Replace(text, "\n", "", -1)
		var command []string
		var b strings.Builder
		var commandOutput string
		var fileRedirect bool
		var redirectArg int
		for i := 0; i < len(text); i++ {
			if text[i] == '\\' && text[i+1] == ' ' {
				b.WriteRune(' ')
				continue
			}
			if text[i] == '\\' && text[i+1] == '\\' {
				b.WriteRune(' ')
				continue
			}
			if text[i] == ' ' {
				command = append(command, b.String())
				b.Reset()
				continue
			}
			if text[i] == '>' {
				commandOutput, _ = run(command)
				fileRedirect = true
				redirectArg = i - 1
				b.Reset()
			}
			b.WriteByte(text[i])
			if i == len(text)-1 {
				command = append(command, b.String())
				b.Reset()
				continue
			}
		}
		if command == nil {
			continue
		}

		if fileRedirect {
			err := ioutil.WriteFile(command[redirectArg], []byte(commandOutput), 0644)
			if err != nil {
				panic(err)
			}
			continue
		}

		commandOutput, _ = run(command)
		fmt.Print(commandOutput)
	}
}

func contains(s string, list []string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

func printPath() string {
	u, err := user.Current()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	s := workingDir
	if strings.HasPrefix(s, u.HomeDir) {
		s = s[len(u.HomeDir):]
		s = "~" + s
	}
	return s
}
