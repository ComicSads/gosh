package main

import (
	"fmt"
	"os"
	"strconv"
)

func exitBuiltin(list []string) {
	if len(list) > 1 {
		n, err := strconv.Atoi(list[1])
		if err == nil {
			os.Exit(n)
		}
		fmt.Println("Exit needs an integer argument")
		os.Exit(1)
	}
	os.Exit(0)
}
