package main

import (
	"fmt"
	"os"
	"os/user"
	"strings"
)

var recentDir string
var workingDir string

func cdBuiltin(list []string) {
	if len(list) == 1 {
		fmt.Println("cd needs an argument")
		return
	}
	newDir := list[1]
	if list[1] == "-" {
		if recentDir == "" {
			fmt.Println("No recent directory")
			return
		}
		newDir = recentDir
	}
	if strings.HasPrefix(newDir, "~") {
		newDir = strings.TrimLeft(newDir, "~")
		u, err := user.Current()
		if err != nil {
			fmt.Println("Couldn't identify home directory")
			return
		}
		newDir = u.HomeDir + newDir
	}
	err := os.Chdir(newDir)
	if err != nil {
		fmt.Println("Unable to change directory")
		fmt.Println(err)
	}
	hold := workingDir
	workingDir, err = os.Getwd()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	recentDir = hold
}
