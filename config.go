package main

var alias = make(map[string]string)
var ps1 string

func config() {
	alias = make(map[string]string)
	alias["sleep"] = "count"
	alias["q"] = "exit"
	alias["echo"] = "neko"
	ps1 = "%cc-> %cg$ "
}
