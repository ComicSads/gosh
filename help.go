package main

import (
	"fmt"
)

func helpBuiltin(list []string) string {
	helpString := "%s:%s\n"
	var toPrint string
	if len(list) == 1 {
		for _, v := range builtins {
			s := fmt.Sprintf(helpString, v.name, v.help)
			toPrint = toPrint + s
		}
	} else {
		for _, v := range list[1:] {
			for i := 0; i < len(builtins); i++ {
				if v == builtins[i].name {
					s := fmt.Sprintf(helpString, builtins[i].name, builtins[i].help)
					toPrint = toPrint + s
				}
			}
		}
	}
	if toPrint == "" {
		toPrint = fmt.Sprintln("Command not found, try using `man command` or `command -h`")
	}
	return toPrint
}
