package main

import (
	"fmt"
	"os/exec"
)

func run(command []string) (s string, e error) {
	if alias[command[0]] != "" {
		command[0] = alias[command[0]]
	}

	if contains(command[0], commands) {
		s = runBuiltin(command)
		return s, nil
	}
	cmd := exec.Command(command[0], command[1:]...)
	output, err := cmd.Output()
	if err != nil {
		if err.Error() == "exec: \""+cmd.Path+"\": executable file not found in $PATH" {
			e = fmt.Errorf("%s not found in path\n", cmd.Path)
			return "", e
		} else {
			return "", err
		}
	}
	return string(output), nil
}

func runBuiltin(list []string) (s string) {
	switch list[0] {
	case "neko":
		s = nekoBuiltin(list)
	case "exit":
		exitBuiltin(list)
	case "ls":
		s = lsBuiltin(list)
	case "cd":
		cdBuiltin(list)
	case "pwd":
		s = fmt.Sprintln(printPath())
	case "type":
		s = typeBuiltin(list)
	case "cat":
		s = catBuiltin(list)
	case "alias":
		s = aliasBuiltin(list)
	case "help":
		s = helpBuiltin(list)
	}
	return s
}
