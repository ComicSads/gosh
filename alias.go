package main

import (
	"fmt"
)

func aliasBuiltin(list []string) string {
	var s string
	if len(list) != 1 {
		s = fmt.Sprintln("Currently, you can only set aliases manually in config.go, this will soon be improved")
		return s
	}
	for k, v := range alias {
		s = fmt.Sprintf("%s is an alias for %s\n", v, k)
	}
	return s
}
