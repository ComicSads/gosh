package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func lsBuiltin(list []string) string {
	var s string
	readDir := workingDir
	var showHidden bool
	if len(list) > 1 {
		err := lsArgParse(list[1:], &readDir, &showHidden)
		if err != nil {
			return ""
		}
	}
	files, err := ioutil.ReadDir(readDir)
	if err != nil {
		fmt.Errorf("Can't read working directory\n")
		os.Exit(1)
	}
	for _, f := range files {
		if showHidden {
			s = s + fmt.Sprintln(f.Name())
		} else if f.Name()[0] != '.' {
			s = s + fmt.Sprintln(f.Name())
		}
	}
	return s
}

func lsArgParse(list []string, readDir *string, showHidden *bool) (e error) {
	var argsDone bool
	for i, v := range list {
		if list[i][0] == '/' {
			*readDir = list[i]
			continue
		}
		switch v {
		case "-a":
			if argsDone {
				continue
			}
			*showHidden = true
		case "--":
			if argsDone {
				continue
			}
			argsDone = true
		default:
			info, err := os.Stat(*readDir + "/" + list[i])
			if !os.IsNotExist(err) {
				if info.IsDir() {
					*readDir = *readDir + "/" + list[i]
				} else {
					fmt.Println(list[i])
					return fmt.Errorf("File is not directory")
				}
			}
		}
	}
	return nil
}
