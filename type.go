package main

import (
	"fmt"
	"os/exec"
)

func typeBuiltin(list []string) string {
	var s string
	if len(list) == 1 {
		s = fmt.Sprintln("type needs an argument")
		return s
	}
	if contains(list[1], commands) {
		s = fmt.Sprintf("%s is a shell builtin\n", list[1])
	} else if alias[list[1]] != "" {
		s = fmt.Sprintf("%s is an alias for %s\n", list[1], alias[list[1]])
	} else {
		path, err := exec.LookPath(list[1])
		if err != nil {
			s = fmt.Sprintf("%s not found\n", list[1])
			return s
		}
		s = fmt.Sprintf("%s is %s\n", list[1], path)
	}
	return s
}
