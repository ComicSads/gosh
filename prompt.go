package main

import (
	//"fmt"
	"github.com/fatih/color"
)

func prompt(ps int) {
	if ps == 1 {
		c := color.New(color.FgWhite)
		for i := 0; i < len(ps1); i++ {
			if ps1[i] == '%' {
				i++
				switch ps1[i] {
				case '%':
					c.Print("%")
				case 'd':
					c.Print(printPath())
				case 'c':
					i++
					switch ps1[i] {
					case 'r':
						c = color.New(color.FgRed)
					case 'g':
						c = color.New(color.FgGreen)
					case 'y':
						c = color.New(color.FgYellow)
					case 'b':
						c = color.New(color.FgBlue)
					case 'm':
						c = color.New(color.FgMagenta)
					case 'c':
						c = color.New(color.FgCyan)
					case 'w':
						c = color.New(color.FgWhite)
					default:
						panic("prompt color invalid")
					}
				default:
					panic("prompt invalid")
				}
			} else {
				c.Print(string(ps1[i]))
			}
		}
	} else {
		panic("incorrect prompt type")
	}
}
